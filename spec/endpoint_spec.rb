RSpec.describe Endpoint do
  subject { described_class.new(id:, verb:, path:, response: response_attributes) }

  let(:id) { '12345' }
  let(:verb) { 'GET' }
  let(:path) { '/hello' }
  let(:response_attributes) { build(:endpoint_response) }

  describe '#initialize' do
    it 'assigns id, verb, path, and response attributes' do
      expect(subject.id).to eq(id)
      expect(subject.verb).to eq(verb)
      expect(subject.path).to eq(path)
      expect(subject.response.code).to eq(response_attributes.code)
      expect(subject.response.headers).to eq(response_attributes.headers)
      expect(subject.response.body).to eq(response_attributes.body)
    end
  end

  describe '#to_h' do
    let(:endpoint) do
      {
        type: 'endpoints',
        id: '12345',
        attributes: {
          verb: 'GET',
          path: '/hello',
          response: {
            code: 200,
            headers: { 'Content-Type' => CONTENT_TYPE_JSON },
            body: '{ "message": "Hello, world" }'
          }
        }
      }
    end

    it 'returns the hash representation of the endpoint' do
      expect(subject.to_h).to eq(endpoint)
    end
  end
end
