require 'dotenv/load'
require 'sinatra/base'
require_relative 'storage'
require_relative 'constants'
require_relative 'jwt_access'
require_relative 'error_handlers'
require_relative 'helpers'
require_relative 'endpoint'
require_relative 'validator'

class BabelEcho < Sinatra::Base
  set :default_content_type, CONTENT_TYPE_API_JSON
  set :show_exceptions, false

  before { authorize_admin! }
  after { response.body = JSON.dump(response.body) }

  helpers Helpers
  register ErrorHandlers

  # get
  get '/endpoints' do
    halt 200, { data: $storage_repository.all }
  end

  # create
  post '/endpoints' do
    payload = JSON.parse(request.body.read)

    endpoint = Endpoint.new
    endpoint.for(payload:)
    endpoint.validate!
    endpoint.validate_non_existance!

    $storage_repository.insert(item: endpoint)

    halt 201, { data: endpoint.to_h }
  end

  # update
  patch '/endpoints/:id' do |id|
    payload = JSON.parse(request.body.read)

    endpoint = Endpoint.new
    endpoint.for(payload:)
    endpoint.validate!
    endpoint.update!(id:, payload:)

    halt 200, { data: endpoint.to_h }
  end

  # delete
  delete '/endpoints/:id' do |id|
    $storage_repository.delete_by!(id:)
    halt 204
  end

  # Dynamic route definitions
  %w[get post put delete patch options].each do |verb|
    send(verb, '/*') { handle_request(request.request_method, request.path_info) }
  end
end
