require 'jwt'

SECRET_KEY = ENV.fetch('SECRET_KEY', SecureRandom.uuid)

class JwtAccess
  def self.generate_jwt(access_group)
    payload = {
      access_group:
    }
    JWT.encode(payload, SECRET_KEY, 'HS256')
  end

  def self.decode_jwt(token)
    decoded_token = JWT.decode(token, SECRET_KEY, true, algorithm: 'HS256')
    decoded_token[0]['access_group']
  rescue JWT::DecodeError
    nil
  end
end
