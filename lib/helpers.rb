module Helpers
  def authorize_admin!
    token = request.env['HTTP_AUTHORIZATION'].split.last
    decoded_data = JwtAccess.decode_jwt(token)
    raise Unauthorized unless decoded_data
    raise Forbidden unless decoded_data == 'admin'
  end

  def handle_request(verb, path)
    endpoint = $storage_repository.find_endpoint_by(verb: verb.upcase, path:)
    return json_response(endpoint[:attributes][:response]) if endpoint

    raise PathNotFound, path
  end

  def json_response(response)
    headers = response[:headers] || {}
    headers['Content-Type'] = CONTENT_TYPE_JSON
    halt response[:code].to_i, headers, response[:body]
  end
end
