describe Validator do
  describe '.validate_json_schema!' do
    context 'with valid payload' do
      let(:valid_payload) do
        {
          'data' => {
            'type' => 'endpoints',
            'attributes' => {
              'verb' => 'GET',
              'path' => '/sample',
              'response' => {
                'code' => 200,
                'headers' => {
                  'Authorization' => 'Bearer: 1234',
                  'Accept-Language' => 'en-US'
                },
                'body' => 'Sample response'
              }
            }
          }
        }
      end

      it 'does not raise an error' do
        expect { described_class.validate_json_schema!(payload: valid_payload) }.not_to raise_error
      end
    end

    context 'with invalid payload' do
      let(:invalid_payload) do
        {
          'data' => {
            'attributes' => {
              'verb' => 'GET',
              'path' => '/sample'
            }
          }
        }
      end

      it 'raises a JSON::Schema::ValidationError' do
        expect do
          described_class.validate_json_schema!(payload: invalid_payload)
        end.to raise_error(JSON::Schema::ValidationError)
      end
    end
  end
end
