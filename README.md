# Mock Server Documentation

This repository houses a mock server. For a detailed breakdown of its functionality, please refer to the [Original Documentation](doc/echo.md).

## Important Notes

- Creation or updating of endpoints beginning with `/endpoints` is prohibited.

## Setting Up the Server

### Running Locally

```plaintext
$ bundle exec rackup
```

### Using Docker

#### Building the Image

```plaintext
$ docker build --build-arg RUBY_VERSION=$(cat .ruby-version) -t babel-echo .
```

#### Running the Container

```plaintext
$ docker run -p 9292:9292 babel-echo
```

#### Generating Authentication Token Inside Docker Container

```plaintext
$ docker exec <container_id> /bin/sh -c 'ruby -r "./lib/babel_echo.rb" -e "puts JwtAccess.generate_jwt(\'admin\')"'
```

## Usage

### Authorization

#### Secret Generation

```plaintext
$ echo "SECRET_KEY=$(openssl rand -base64 36)" > .env
```

#### Authentication Token Generation

```plaintext
$ ruby -r "./lib/babel_echo.rb" -e "puts JwtAccess.generate_jwt('admin')"
```

### Endpoint Operations

#### Fetching List of Mock Endpoints

**Request:**

```plaintext
$ curl -v -H "Authorization: Bearer <TOKEN>" -X GET http://localhost:9292/endpoints | json_pp
```

**Response:**

```plaintext
{
    "data" : []
}
```

#### Creating a Mock Endpoint

**Request:**

```plaintext
$ curl -X POST -H "Authorization: Bearer <TOKEN>" -H "Accept: application/vnd.api+json" -d '{"data":{"type":"endpoints","attributes":{"verb":"GET","path":"/sample","response":{"code":200,"headers":{"Authorization":"Bearer: 1234","Accept-Language":"en-US"},"body":"Sample response"}}}}' http://localhost:9292/endpoints | json_pp
```

**Success Response:**

```plaintext
> POST /endpoints HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json
> Content-Type: application/x-www-form-urlencoded

< HTTP/1.1 201 Created
< Content-Type: application/vnd.api+json

{
    "data" : {
        "attributes" : {
            "path" : "/sample",
            "response" : {
                "body" : "Sample response",
                "code" : 200,
                "headers" : {
                    "Accept-Language" : "en-US",
                    "Authorization" : "Bearer: 1234"
                }
            },
            "verb" : "GET"
        },
        "id" : "84cf1846-780b-48ff-92dd-86941bec3117",
        "type" : "endpoints"
    }
}
```

#### Updating a Mock Endpoint

Update the mock endpoint by its `id` with the new mock payload.

**Request:**

```plaintext
$ curl -s -v -X PATCH -H "Authorization: Bearer <TOKEN>" -H "Accept: application/vnd.api+json" -d '{"data":{"type":"endpoints","attributes":{"verb":"GET","path":"/sample","response":{"code":200,"headers":{"Authorization":"Bearer: 1234","Accept-Language":"en-US"},"body":"Sample response modified"}}}}' http://localhost:9292/endpoints/84cf1846-780b-48ff-92dd-86941bec3117 | json_pp
```

**Success Response:**

```plaintext
> PATCH /endpoints/84cf1846-780b-48ff-92dd-86941bec3117 HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json
> Content-Length: 201
> Content-Type: application/x-www-form-urlencoded

< HTTP/1.1 200 OK
< Content-Type: application/vnd.api+json

{
    "data" : {
        "attributes" : {
            "path" : "/sample",
            "response" : {
                "body" : "Sample response modified",
                "code" : 200,
                "headers" : {
                "Accept-Language" : "en-US",
                "Authorization" : "Bearer: 1234"
                }
            },
            "verb" : "GET"
        },
        "id" : "84cf1846-780b-48ff-92dd-86941bec3117",
        "type" : "endpoints"
    }
}

```

**Failure Response:**

```plaintext
> PATCH /endpoints/84cf1846-780b-48ff-92dd-86941bec3117 HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json

< HTTP/1.1 404 Not Found
< Content-Type: application/vnd.api+json

{
    "errors" : [
        {
            "code" : "not_found",
            "detail" : "Requested Endpoint with ID  `84cf1846-780b-48ff-92dd-86941bec3117` does not exist"
        }
    ]
}

```

#### Deleting a Mock Endpoint

Delete an endpoint using its `id`. A successful deletion will result in a `204 No Content` response.

**Request:**

```plaintext
$ curl -v -X DELETE -H "Authorization: Bearer <TOKEN>" -H "Accept: application/vnd.api+json" http://localhost:9292/endpoints/84cf1846-780b-48ff-92dd-86941bec3117 | json_pp
```

**Success Response:**

```plaintext
> DELETE /endpoints/ef03f6f7-0afb-4ecb-aa3d-eb354bc49f7a HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json

< HTTP/1.1 204 No Content
```

**Failure Response:**

```plaintext
> DELETE /endpoints/ef03f6f7-0afb-4ecb-aa3d-eb354bc49f7a1 HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json

< HTTP/1.1 404 Not Found
< Content-Type: application/vnd.api+json

{
    "errors" : [
            {
                "code" : "not_found",
                "detail" : "Requested Endpoint with ID `ef03f6f7-0afb-4ecb-aa3d-eb354bc49f7a1` does not exist"
            }
        ]
}
```

#### Arbitrary Mocked Endpoint Call

**Request:**

```plaintext
$ curl -v -H "Authorization: Bearer <TOKEN>" -X GET http://localhost:9292/my_endpoint | json_pp
```

**Success Response:**

```plaintext
> POST /endpoints HTTP/1.1
> Host: localhost:9292
> Accept: application/vnd.api+json
> Content-Type: application/x-www-form-urlencoded

< HTTP/1.1 201 Created
< Content-Type: application/vnd.api+json

{
    "data" : {
        "attributes" : {
            "path" : "/my_endpoint",
            "response" : {
                "body" : "Sample response",
                "code" : 200,
                "headers" : {
                    "Accept-Language" : "en-US",
                    "Authorization" : "Bearer: 1234"
                }
            },
            "verb" : "GET"
        },
        "id" : "84cf1846-780b-48ff-92dd-86941bec3117",
        "type" : "endpoints"
    }
}
```

**Failure Response:**

```plaintext
{
    "errors" : [
        {
            "code" : "not_found",
            "detail" : "Requested path `/my_endpoint` does not exist"
        }
    ]
}
```

### Running Tests

```plaintext
$ bundle exec rspec
```