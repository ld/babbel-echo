# A simple in-memory store for our mock endpoints.
# In a real application, this could be a database.

$storage = {}

class StorageRepository
  attr_reader :id, :db

  def initialize(db = $storage)
    @db = db
  end

  def find_by!(id:)
    @id = id
    @db[id] || raise(NotFound, id)
  end

  def find_endpoint_by(verb:, path:)
    @db.values.map(&:to_h).find do |obj|
      obj[:attributes][:verb].casecmp?(verb) && obj[:attributes][:path].casecmp?(path)
    end
  end

  def insert(item:)
    @db[item.id] = item
  end

  def update!(id:, payload:)
    obj = find_by!(id:)
    obj.verb = payload['data']['attributes']['verb']
    obj.path = payload['data']['attributes']['path']
    obj.response = payload['data']['attributes']['response']
    obj
  end

  def delete_by!(id:)
    raise NotFound, id unless @db.delete(id)
  end

  def all
    @db.values.map(&:to_h)
  end
end

$storage_repository = StorageRepository.new
