RESERVED_PATH_REGEX = %r{\A/endpoints}i
CONTENT_TYPE_JSON = 'application/json'.freeze
CONTENT_TYPE_API_JSON = 'application/vnd.api+json'.freeze
