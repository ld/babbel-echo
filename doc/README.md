# Code challenge for backend developers (TNT edition)

A code challenge offered to backend engineers applying for an open
position at Tech'n'Tools@Babbel. The main purpose of the challenge is to give
you the opportunity to demonstrate us your engineering skills.
Your implementation of the Echo is used later as part of the technical interview.

## Where to find the challenge description?

A full technical specification and a couple of examples are provided in
[echo.md][echo.md]. Take your time to read through it carefully.

## How to deliver results?

It isn't necessary to deploy your solution anywhere. It's enough to send us a
link to *a public* repository on GitHub or GitLab. README should include
instructions on how to run and test the project on a local machine. Ideally, it
should only require a couple of commands to make the server up and running.

We will have Ruby and Bundler installed on our machines, so there is no need to
explain them.

## Do NOT

* Do NOT violate [copyrights](https://en.wikipedia.org/wiki/Copyright) in your
  submission
* Do NOT share [sensitive
  information](https://en.wikipedia.org/wiki/Information_sensitivity) in your
  submission
* Do NOT share/redistribute the challenge in any medium or format other than described in the previous section
* Do NOT copy or base your solution on other solutions available on github as we will definitely spot this and most of the other solutions were unsuccessful anyway ;)

##  Have questions?

In case something isn't clear or you have questions regarding the task, feel
free to contact us. Email address: <tnt@babbel.com>

---

Thank you for being awesome and have fun with the challenge!
