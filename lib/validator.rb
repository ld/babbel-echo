require 'json-schema'

class Validator
  @json_schema = {
    '$schema' => 'http://json-schema.org/draft-04/schema#',
    'type' => 'object',
    'properties' => {
      'data' => {
        'type' => 'object',
        'properties' => {
          'type' => { 'type' => 'string' },
          'attributes' => {
            'type' => 'object',
            'properties' => {
              'verb' => {
                'type' => 'string',
                'enum' => %w[GET POST PUT DELETE PATCH OPTIONS]
              },
              'path' => { 'type' => 'string' },
              'response' => {
                'type' => 'object',
                'properties' => {
                  'code' => { 'type' => 'integer' },
                  'headers' => { 'type' => 'object' },
                  'body' => { 'type' => 'string' }
                },
                'required' => ['code']
              }
            },
            'required' => %w[verb path response]
          }
        },
        'required' => %w[type attributes]
      }
    },
    'required' => ['data']
  }

  def self.validate_json_schema!(payload:, json_schema: @json_schema)
    JSON::Validator.validate!(json_schema, payload)
  end
end
