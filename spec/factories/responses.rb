FactoryBot.define do
  factory :endpoint_response, class: Endpoint::Response do
    code { 200 }
    headers { { 'Content-Type' => CONTENT_TYPE_JSON } }
    body { '{ "message": "Hello, world" }' }
  end
end

FactoryBot.define do
  factory :post_endpoints_response, class: Array do
    initialize_with do
      [
        {
          'attributes' => {
            'path' => '/sample',
            'response' => {
              'body' => 'Sample response',
              'code' => 200,
              'headers' => {
                'Accept-Language' => 'en-US',
                'Authorization' => 'Bearer: 1234'
              }
            },
            'verb' => 'GET'
          },
          'id' => '123',
          'type' => 'endpoints'
        }
      ]
    end

    trait :modified do
      initialize_with do
        [
          {
            'attributes' => {
              'path' => '/modified',
              'response' => {
                'body' => 'Modified response',
                'code' => 200,
                'headers' => {}
              },
              'verb' => 'GET'
            },
            'id' => '123',
            'type' => 'endpoints'
          }
        ]
      end
    end
  end
end
