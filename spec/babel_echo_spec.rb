RSpec.describe BabelEcho, type: :request do
  include Rack::Test::Methods

  subject { last_response }
  let(:json_body) { JSON.parse(subject.body) }

  let(:admin_token) { JwtAccess.generate_jwt('admin') }
  let(:invalid_token) { 'invalid_token' }

  def app
    described_class
  end

  before do
    header 'Accept', CONTENT_TYPE_API_JSON
    header 'Authorization', "Bearer #{admin_token}"
  end

  after(:each) { $storage.clear }
  let(:endpoint) { build(:endpoint) }

  describe 'GET /endpoints' do
    context 'when there are no endpoints' do
      before { get '/endpoints' }

      it 'responds with a 200 status code' do
        expect(subject).to be_ok
      end

      it 'returns an empty list' do
        expect(json_body['data']).to be_empty
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
      end
    end

    context 'when there are some endpoints' do
      before do
        $storage[endpoint.id] = endpoint
        get '/endpoints'
      end

      let(:expected_response) { build(:post_endpoints_response) }

      it 'responds with a 200 status code' do
        expect(subject).to be_ok
      end

      it 'returns the expected response data' do
        expect(json_body['data']).to eq(expected_response)
      end
    end

    context 'with an invalid token' do
      before { header 'Authorization', invalid_token }

      it 'responds with a 401 Unauthorized error' do
        get '/endpoints'
        expect(subject.status).to eq(401)
      end

      it "returns the 'Unauthorized' error message" do
        get '/endpoints'
        expect(json_body).to eq({ 'errors' => [{ 'code' => 'access_unauthorized', 'detail' => 'Unauthorized' }] })
      end
    end
  end

  describe 'POST /endpoints' do
    subject { post '/endpoints', endpoint_data.to_json }
    let(:endpoint_data) { build(:post_endpoints_request).deep_stringify_keys }

    context 'when validation is valid' do
      it 'creates a new endpoint' do
        subject
        json_body['data'].delete('id')

        expect(subject.status).to eq(201)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body).to eq(endpoint_data)
      end

      it 'creates a new endpoint without header and body' do
        endpoint_data['data']['attributes']['response'].delete('body')
        endpoint_data['data']['attributes']['response'].delete('headers')
        subject
        json_body['data'].delete('id')
        json_body['data']['attributes']['response'].delete('body')
        json_body['data']['attributes']['response'].delete('headers')

        expect(subject.status).to eq(201)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body).to eq(endpoint_data)
      end

      it 'creates a new second same endpoint' do
        $storage[endpoint.id] = endpoint
        subject

        expect(subject.status).to eq(403)
        expect(json_body).to eq(
          { 'errors' => [{
            'code' => 'already_exists',
            'detail' => 'Requested Endpoint with verb `GET` and path `/sample` already exists'
          }] }
        )
      end
    end

    context 'when endpoints is invalid' do
      it 'creates a new endpoint' do
        endpoint_data['data']['attributes'].delete('verb')
        subject

        expect(subject.status).to eq(400)
        expect(json_body).to eq(
          { 'errors' => [{
            'code' => 'validation_error',
            'detail' => "The property '#/data/attributes' did not contain a required property of 'verb'"
          }] }
        )
      end
    end

    context 'when endpoint starts with /endpoints' do
      it 'returns 409 error' do
        endpoint_data['data']['attributes']['path'] = '/endpoints'
        subject

        expect(subject.status).to eq(409)
        expect(json_body).to eq({
                                  'errors' => [{
                                    'code' => 'path_reserved',
                                    'detail' => '`/endpoints` is reserved. Please use another one.'
                                  }]
                                })
      end
    end
  end

  describe 'PATCH /endpoints/:id' do
    let(:endpoint_data) { build(:post_endpoints_request, :modified).deep_stringify_keys }

    let(:error_response) do
      { 'errors' => [{ 'code' => 'not_found',
                       'detail' => 'Requested Endpoint with ID `NONE_EXISTANT` does not exist' }] }
    end

    before do
      $storage['123'] = endpoint
      header 'Content-Type', CONTENT_TYPE_API_JSON
    end

    context 'when validation is valid' do
      it 'updates an existing endpoint' do
        patch '/endpoints/123', endpoint_data.to_json
        expect(subject.status).to eq(200)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body['data']['attributes']['path']).to eq('/modified')
        expect(json_body['data']['attributes']['response']['body']).to eq('Modified response')
      end
    end

    context 'when validation is invalid' do
      it 'does not updates an existing endpoint' do
        endpoint_data['data']['attributes'].delete('verb')
        patch '/endpoints/123', endpoint_data.to_json

        expect(subject.status).to eq(400)
        expect(json_body).to eq(
          { 'errors' => [{
            'code' => 'validation_error',
            'detail' => "The property '#/data/attributes' did not contain a required property of 'verb'"
          }] }
        )
      end
    end

    context 'when endpoint does not exist' do
      it 'throws 404 error' do
        patch '/endpoints/NONE_EXISTANT', endpoint_data.to_json
        expect(subject.status).to eq(404)
        expect(json_body).to eq(error_response)
      end
    end

    context 'when endpoint starts with /endpoints' do
      it 'returns 409 error' do
        endpoint_data['data']['attributes']['path'] = '/endpoints'
        patch '/endpoints/123', endpoint_data.to_json

        expect(subject.status).to eq(409)
        expect(json_body).to eq({
                                  'errors' => [{
                                    'code' => 'path_reserved',
                                    'detail' => '`/endpoints` is reserved. Please use another one.'
                                  }]
                                })
      end
    end
  end

  describe 'DELETE /endpoints/:id' do
    let(:error_response) do
      { 'errors' => [{ 'code' => 'not_found',
                       'detail' => 'Requested Endpoint with ID `NONE_EXISTANT` does not exist' }] }
    end

    before do
      $storage['123'] = endpoint
    end

    it 'deletes an existing endpoint' do
      delete '/endpoints/123'
      expect(subject.status).to eq(204)
      expect(subject.body).to be_empty
      expect(subject.headers['Content-Type']).to be_nil
      expect($storage['123']).to be_nil
    end

    it 'deletes a non existing endpoint' do
      delete '/endpoints/NONE_EXISTANT'
      expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
      expect(subject.status).to eq(404)
      expect(json_body).to eq(error_response)
    end
  end

  describe 'Arbitrary mocked endpoints' do
    context 'when the endpoint is defined' do
      before do
        endpoint1 = build(:endpoint, id: 'uuid123', verb: 'POST', path: '/foo/bar/baz',
                                     response: { code: 200,
                                                 headers: {
                                                   'Authorization' => 'Bearer: 1234',
                                                   'Accept-Language' => 'de'
                                                 },
                                                 body: { message: 'Sample response' } })
        endpoint2 = build(:endpoint, id: 'uuid124', verb: 'GET', path: '/sample123',
                                     response: {
                                       code: 888,
                                       headers: nil,
                                       body: nil
                                     })
        $storage[endpoint1.id] = endpoint1
        $storage[endpoint2.id] = endpoint2
      end

      it 'responds to a matching verb and path on endpoint1' do
        post '/foo/bar/baz'
        expect(subject.status).to eq(200)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_JSON)
        expect(json_body).to eq({ 'message' => 'Sample response' })
      end

      it 'responds to a matching verb and path on endpoint2' do
        get '/sample123'
        expect(subject.status).to eq(888)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_JSON)
        expect(json_body).to be_empty
      end

      it 'does not respond to a non-matching verb' do
        get '/foo/bar/baz'
        expect(subject.status).to eq(404)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body).to include({ 'errors' => [{ 'code' => 'not_found',
                                                      'detail' => 'Requested path `/foo/bar/baz` does not exist' }] })
      end

      it 'does not respond to a non-matching path' do
        post '/foo/bar'
        expect(subject.status).to eq(404)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body).to include({ 'errors' => [{ 'code' => 'not_found',
                                                      'detail' => 'Requested path `/foo/bar` does not exist' }] })
      end
    end

    context 'when the endpoint is not defined' do
      it 'returns a 404 error' do
        post '/undefined/path'
        expect(subject.status).to eq(404)
        expect(subject.headers['Content-Type']).to eq(CONTENT_TYPE_API_JSON)
        expect(json_body).to include({
                                       'errors' => [{
                                         'code' => 'not_found',
                                         'detail' => 'Requested path `/undefined/path` does not exist'
                                       }]
                                     })
      end
    end
  end
end
