RSpec.describe JwtAccess do
  describe '.generate_jwt' do
    subject(:generated_token) { described_class.generate_jwt(access_group) }

    let(:access_group) { 'admin' }

    it 'is not nil' do
      expect(generated_token).not_to be_nil
    end

    it 'contains the expected access_group when decoded without verification' do
      decoded_payload = JWT.decode(generated_token, SECRET_KEY, false)
      expect(decoded_payload[0]['access_group']).to eq(access_group)
    end
  end

  describe '.decode_jwt' do
    subject(:decoded_access_group) { described_class.decode_jwt(token) }

    context 'when given a valid token' do
      let(:access_group) { 'admin' }
      let(:token) { described_class.generate_jwt(access_group) }

      it { is_expected.to eq(access_group) }
    end

    context 'when given an invalid token' do
      let(:token) { 'invalid.token.here' }

      it { is_expected.to be_nil }
    end

    context 'when given a token with a wrong secret' do
      let(:access_group) { 'admin' }
      let(:token) { JWT.encode({ access_group: }, 'wrong_secret', 'HS256') }

      it { is_expected.to be_nil }
    end
  end
end
