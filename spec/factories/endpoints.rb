FactoryBot.define do
  factory :endpoint do
    id { '123' }
    verb { 'GET' }
    path { '/sample' }
    response do
      {
        code: 200,
        headers: {
          'Authorization' => 'Bearer: 1234',
          'Accept-Language' => 'en-US'
        },
        body: 'Sample response'
      }
    end

    initialize_with { new(verb:, path:, response:) }

    trait :modified do
      path { '/modified' }
      response do
        {
          code: 200,
          headers: {},
          body: 'Modified response'
        }
      end
    end

    trait :with_different_path do
      sequence(:path) { |n| "/sample#{n}" }
    end
  end
end
