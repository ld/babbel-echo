ARG RUBY_VERSION

FROM ruby:${RUBY_VERSION}

WORKDIR /app
COPY . /app

RUN gem install bundler && bundle install

CMD ["bundle", "exec", "rackup", "-o", "0.0.0.0"]
