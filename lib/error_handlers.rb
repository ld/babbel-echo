class NotFound < StandardError; end
class AlreadyExists < StandardError; end
class PathReserved < StandardError; end
class PathNotFound < StandardError; end
class Unauthorized < StandardError; end
class Forbidden < StandardError; end

module ErrorHandlers
  def self.registered(app) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    # Error handling
    app.error PathReserved do
      halt 409, { errors: [{ code: 'path_reserved', detail: '`/endpoints` is reserved. Please use another one.' }] }
    end

    app.error AlreadyExists do |e|
      verb, path = e.message.split(',')
      detail = "Requested Endpoint with verb `#{verb}` and path `#{path}` already exists"
      halt 403, { errors: [{ code: 'already_exists', detail: }] }
    end

    app.error NotFound do |e|
      halt 404, { errors: [{ code: 'not_found', detail: "Requested Endpoint with ID `#{e.message}` does not exist" }] }
    end

    app.error PathNotFound do |e|
      detail = "Requested path `#{e.message}` does not exist"
      error = { errors: [{ code: 'not_found', detail: }] }
      halt 404, { 'Content-Type' => CONTENT_TYPE_API_JSON }, error
    end

    app.error JSON::ParserError do |e|
      halt 400, { errors: [{ code: 'json_parser_error', detail: e.message }] }
    end

    app.error JSON::Schema::ValidationError do |e|
      halt 400, { errors: [{ code: 'validation_error', detail: e.message }] }
    end

    app.error Unauthorized do
      halt 401, { errors: [{ code: 'access_unauthorized', detail: 'Unauthorized' }] }
    end

    app.error Forbidden do
      halt 403, { errors: [{ code: 'access_forbidden', detail: 'Forbidden' }] }
    end
  end
end
