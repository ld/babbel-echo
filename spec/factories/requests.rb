FactoryBot.define do
  factory :post_endpoints_request, class: Hash do
    data do
      {
        type: 'endpoints',
        attributes: {
          verb: 'GET',
          path: '/sample',
          response: {
            code: 200,
            headers: {
              'Authorization' => 'Bearer: 1234',
              'Accept-Language' => 'en-US'
            },
            body: 'Sample response'
          }
        }
      }
    end

    initialize_with { attributes }

    trait :modified do
      data do
        {
          type: 'endpoints',
          attributes: {
            verb: 'GET',
            path: '/modified',
            response: {
              code: 200,
              headers: {},
              body: 'Modified response'
            }
          }
        }
      end
    end
  end
end
