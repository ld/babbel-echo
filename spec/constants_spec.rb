RSpec.describe 'Constants' do
  describe 'RESERVED_PATH_REGEX' do
    it { expect(RESERVED_PATH_REGEX).to eq(%r{\A/endpoints}i) }
  end

  describe 'CONTENT_TYPE_JSON' do
    it { expect(CONTENT_TYPE_JSON).to eq('application/json') }
  end

  describe 'CONTENT_TYPE_API_JSON' do
    it { expect(CONTENT_TYPE_API_JSON).to eq('application/vnd.api+json') }
  end
end
