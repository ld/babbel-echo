require 'securerandom'

class Endpoint
  attr_accessor :id, :verb, :path, :response

  def initialize(id: SecureRandom.uuid, verb: nil, path: nil, response: Response.new)
    @id = id
    @verb = verb
    @path = path
    @response = response
  end

  def for(payload:) # rubocop:disable Metrics/AbcSize
    @payload = payload
    @id = SecureRandom.uuid
    @verb = payload['data']['attributes']['verb']
    @path = payload['data']['attributes']['path']
    @response = Response.new(
      code: payload['data']['attributes']['response']['code'],
      headers: payload['data']['attributes']['response']['headers'],
      body: payload['data']['attributes']['response']['body']
    )
  end

  def validate!
    Validator.validate_json_schema!(payload: @payload)
    validate_path!(payload: @payload)
  end

  def update!(id:, payload:)
    $storage_repository.update!(id:, payload:)
  end

  def validate_path!(payload:)
    raise PathReserved if payload['data']['attributes']['path'].match?(RESERVED_PATH_REGEX)
  end

  def validate_non_existance!
    verb = @payload['data']['attributes']['verb']
    path = @payload['data']['attributes']['path']
    raise AlreadyExists, [verb, path].join(',') if $storage_repository.find_endpoint_by(verb:, path:)
  end

  def to_h
    {
      type: 'endpoints',
      id: @id,
      attributes: {
        verb: @verb,
        path: @path,
        response: @response.to_h
      }
    }
  end

  # Nested Response class to handle the response structure
  class Response
    attr_accessor :code, :headers, :body

    def initialize(code: nil, headers: nil, body: nil)
      @code = code
      @headers = headers
      @body = body
    end

    def to_h
      {
        code: @code,
        headers: @headers,
        body: @body
      }
    end
  end
end
